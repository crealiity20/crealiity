var markerObject;

// HTML template for the mediabox

var markup_mediabox = '\
        <div class="${column}">\
            <div class="boxContent">\
                <a class="indicator1" href="#"></a>\
                <p class="dateShow">${dateshow}</p>\
                <p class="timeShow">${timeShow}</p>\
                <img class="imageTop clearfix" src="images/image-1.png" />\
                <img class="profile" src="${profile}" />\
                <div class="textWrap">\
                    <p class="text"><a href="#"><img src="images/icons/dropdown.png" href="#" /></a>\
                        &nbsp;<strong>${name}</strong>${text}</p>\
                </div>\
            </div>\
            <div class="boxContentDown">\
                <img class="profile" src="images/profile-2.png" />\
                <div class="textWrap">\
                    <p><a><img src="images/icons/comment-profile.png" href="#" /></a>&nbsp;<strong>Walter Scheuch</strong>  Wow! Ein Freund von mir möchte ein Restaurant eröffnen. Soll ich dir den Kontakt geben?</p>\
                </div>\
            </div>\
        </div>';

// HTML template for the search box

var markup_search = '\
         <li class="clearfix">\
            <a class="filter ${filterType}" href="#">&nbsp;</a>\
            <a class="${favorite}" href="#">&nbsp;</a>\
            <a class="${messageCheck}" href="#">&nbsp;</a>\
            <div class="menuSearch">&nbsp;</div>\
            <div class="holder clearfix">\
                <a class="link" href="#">${name}</a>\
                <p>${text}</p>\
            </div>\
            <a class="remove" href="#">remove</a>\
        </li>\
';

// fill the mediabox on keyup from the search/post box

$('body').keyup(function (e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        if ($('.searchPostBoxInner').val() != 0) {
            fillMediaBox();
        }
    }
});

// change classes

$('.media-slider a, .group-by a').on({
    click:function(){
        $(this).siblings('a').removeClass('active');
        $(this).addClass('active');
    }
});

// toggle the class and fill the mediabox

$('.offenCheck a').on({
    click:function(){
        $(this).toggleClass('active');
        fillMediaBox();
    }
});

// filling the mediabox with data

function fillMediaBox() {

    var intranetArray = [];
    $('.root a.checkLight').each(function(index){
        intranetArray.push($(this).data('id'));
    });
    var intranet = intranetArray.join();
    //console.log(intranet);

    $.ajax({
        type    :'GET',
        url     :'http://dev.websafari-secure.eu/miladin/ajaxResponse/getPosts.php',
        data    : {
            'searchword'        : $('.searchPostBoxInner').val(),
            'tabUp'             : $('.media-slider a.active').data('id'),
            'tabDown'           : $('.group-by a.active').data('id'),
            'intranet'          : intranet,
            'offen'             : $('.offenCheck a.active').data('id')

        },
        success : function(response) {
            $('.panel-container').html('');
            console.log(response);
            markerObject = eval(response);
            for (var i in markerObject){
                if((parseInt(i)+1)%3 == 1){
                    markerObject[i].column = 'column-one';
                    $('.panel-container').append('<div class="panel"><div class="panel-wrapper"><div class="main clearfix"></div></div></div>');
                }
                if((parseInt(i)+1)%3 == 2){
                    markerObject[i].column = 'column-two';
                }
                if((parseInt(i)+1)%3 == 0){
                    markerObject[i].column = 'column-three';
                }

                var currentWrapper = parseInt((parseInt(i))/3);
                $.template('posts', markup_mediabox );
                $('.panel-wrapper .main').eq(currentWrapper).append($.tmpl('posts', markerObject[i]));
            }
        }
    });
}

// filling the mediabox with data

function fillSearchBox() {

    $.ajax({
        type    :'GET',
        url     :'http://dev.websafari-secure.eu/miladin/ajaxResponse/getPostsBottom.php',
        data    : {
            'searchRes' :     $('.search-filter a.active').data('id')
        },
        success : function(response) {
            $('.navBox ul').html('');
            console.log(response);
            markerObject = eval(response);
            for (var i in markerObject){
                markerObject[i].messageCheck = markerObject[i].messageCheck ? 'dot' : 'nothing';
                markerObject[i].favorite = markerObject[i].favorite ? 'favorite' : 'nothing';
                markerObject[i].filterType = $('.search-filter a.active').attr('class').split(' ')[0];
                markerObject[i].text = $.trim('Magna auguero dolobor seniscin utet lor ing ero et lore molobor sustin.')
                $.template('posts', markup_search);
                $('.navBox ul').append($.tmpl('posts', markerObject[i]));
            }
        }
    });
}


$(document).ready(function(){

    $('.search-filter a').on({
        click:function(e){
            e.preventDefault();
            $(this).siblings('a').removeClass('active');
            $(this).addClass('active');
            fillSearchBox();
        }
    });

    $('.root a').on({
        click:function(e){
            e.preventDefault();
            fillMediaBox();
        }
    });

});

// delete single result row on search box

$('.remove').on({
    click:function(e){
        $(this).parents('li').remove();
    }
})