/**
 * Flash messages system for crealiity
 *
 * @author  Florian Rachor <fr@websafari.co>
 */

// Initialize navigation object
var navigation = {};

navigationHandler = function(options) {
    // set up default options
    var defaults = {
        selectorSearchIcon:     '.search',
        iconsClassBase:     'icons-menu-'
    };

    // Overwrite default options with user provided ones
    var options = $.extend({}, defaults, options);

    navigation.changeOpacity = function(e, value){
        e.css('opacity', value);
    }

    navigation.changeSearchIcon = function(iconClass) {
        var e = $(options.selectorSearchIcon);
        var itemsCount = $('header li').length;

        // Remove existing icons class
        for(var i = 1; i <= itemsCount; i++) {
            e.removeClass(options.iconsClassBase + i);
        }

        // Add new class
        e.addClass(iconClass);
    }
}

$(document).ready(function(){
    navigationHandler();
});