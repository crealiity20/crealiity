$(document).ready(function(){

    // Autogrow of search input field
    if( $('#search-input') ){
        $('#search-input').autoGrow();
    }

    // Carousel
    var base = $('#mediabox .navBox').length;
    var carouselWidthMediabox = 876 * base;
    $('#mediabox .move').css('width',carouselWidthMediabox + 'px');

    var base = $('#search .navBox').length;
    var carouselWidthSearch = 876 * base;
    $('#search .move').css('width',carouselWidthSearch + 'px');

    $('.ajax-navigation a, .carousel nav a, .carousel .left, .carousel .right').on({
        click:function(e){
            e.preventDefault();
        }
    });

    $('#right').on({
        click: function(e){

            e.preventDefault();

            var latest = $(this).data("time") || 0;
            var now = +new Date();
            var diff = now-latest;

            if(diff > 300){

                $(this).data("time",now);

                var margin = $('#mediabox .move').css('margin-left');
                margin = margin.replace('px', "");
                var newMargin = margin - 876;

                if( margin == -carouselWidthMediabox + 876 ){
                    // nothing so far
                } else {
                    $('#mediabox .move').animate({'margin-left': newMargin + 'px'}, 300);
                }
            }
        }
    });

    $('#left').on({
        click: function(e){

            e.preventDefault();

            var latest = $(this).data("time") || 0;
            var now = +new Date();
            var diff = now-latest;

            if(diff > 300){

                $(this).data("time",now);

                var margin = $('#mediabox .move').css('margin-left');
                margin = margin.replace('px', "");
                var newMargin = parseInt(margin) + 876;

                if( margin === "0" || margin === "0px" ){
                    // nothing so far
                } else {
                    $('#mediabox .move').animate({'margin-left': newMargin + 'px'}, 300);
                }
            }
        }
    });

    $('#mediabox nav a').on({
        click: function(){
            var number = $(this).prevAll().length;
            var prefix = 0;

            for(var i = 0; i < number; i++){
                var dateBox = $('#mediabox .dateBox').eq(i);
                prefix = prefix + $(dateBox).find('.navBox').length;
            }

            var newMargin = -876*prefix;

            $('#mediabox .move').animate({'margin-left': newMargin + 'px'}, 300);
        }
    });

    $('#search .right').on({
        click: function(e){
            e.preventDefault();

            var latest = $(this).data("time") || 0;
            var now = +new Date();
            var diff = now-latest;

            if(diff > 300){

                $(this).data("time",now);

                var margin = $('#search .move').css('margin-left');
                margin = margin.replace('px', "");
                var newMargin = margin - 876;

                if( margin == -carouselWidthSearch + 876 ){
                    // nothing so far
                } else {
                    $('#search .move').animate({'margin-left': newMargin + 'px'}, 300);
                }
            }
        }
    });

    $('#search .left').on({
        click: function(e){
            e.preventDefault();

            var latest = $(this).data("time") || 0;
            var now = +new Date();
            var diff = now-latest;

            if(diff > 300){

                $(this).data("time",now);

                var margin = $('#search .move').css('margin-left');
                margin = margin.replace('px', "");
                var newMargin = parseInt(margin) + 876;

                if( margin === "0" || margin === "0px" ){
                    // nothing so far
                } else {
                    $('#search .move').animate({'margin-left': newMargin + 'px'}, 300);
                }
            }
        }
    });

    $('header nav a').on({
        click: function(e){
            e.preventDefault();
            fillMediaBox();
        }
    });

    $('.media-slider .main a').on({
        click: function(e){
            e.preventDefault();
            fillMediaBox();
        }
    });

    // on click enywhere but..
    $("body").on("click", function() {
        $('.menu').hide();
    });

    $(".menu, .options").on("click", function(e) {
        e.stopPropagation();
        // open div
    });

    // scroling inside mediabox and showing comments
    $('.containerPostings').each(function(){

       var mouse = 0;

       $(this).on({
           mouseenter: function(){

               //comments
               $(this).find('.options-menu').on({
                  click: function(e){
                      e.preventDefault();

                      // hide other comment containers
                      $('.container-right').removeClass('active');
                      $('.column').css({'width': '289px'});

                      $('.menu').hide();
                      var time = $(this).parent().parent().parent().parent().find('.time').height();
                      var mainImage = $(this).parent().parent().parent().parent().find('.main-image').height();
                      var menu = $(this).parent().parent().parent().parent().parent().parent().parent().find('.menu');
                      var top = + time + mainImage + 6 + mouse*10 + 15;

                      menu.css('top', top + 'px');
                      menu.show();

                      if( $(this).parent().parent().parent().parent().parent().parent().hasClass('last') ){
                          menu.css('left', -400 + 'px');
                          menu.addClass('arrowChange');
                      }
                  }
               });

               // mousewhell functionality
               $(this).bind('mousewheel', function(event, delta) {
                   $('.menu').hide();
                   var height = $(this).height();
                   document.body.style.overflow=false?"":"hidden";
                   var dir = delta > 0 ? 'Up' : 'Down',
                       vel = Math.abs(delta);

                   var marker = height - 330;

                   if(dir == "Up"){

                       mouse = mouse + vel;

                       if(mouse*10 > 0){

                           mouse = 0;

                           $(this).css({
                               'top': mouse*10 + 'px'
                           });
                       } else {

                           $(this).css({
                               'top': mouse*10 + 'px'
                           });
                       }
                   } else {

                       mouse = mouse - vel;

                       if(-mouse*10 > marker){

                           mouse = -marker/10;

                           $(this).css({
                               'top': mouse*10 + 'px'
                           });
                       } else {

                           $(this).css({
                               'top': mouse*10 + 'px'
                           });
                       }
                   }
               });
           },

           // retunr to normal mousewheel
           mouseleave: function(){
               document.body.style.overflow=true?"":"hidden";
           }
       });
    });

    $('.single').on({
        click: function(){
            $('.single').removeClass('active');
            $(this).addClass('read').addClass('active');
        }
    });

    // slider for messages
    $('.nachricten').on({
        click:function(){
            $('header nav, .menuIndicator').hide();
            $('.message-slider').show();
        }
    });

    $('.postings').on({
       click: function(){
           $('header nav, .menuIndicator').show();
           $('.message-slider').hide();
       }
    });

    var select = $( "#messageSlider" );
    var slider = $( "<div id='slider'></div>" ).insertAfter( select ).slider({
        min: 1,
        max: 8,
        range: "min",
        value: select[ 0 ].selectedIndex + 1,
        slide: function( event, ui ) {
            select[ 0 ].selectedIndex = ui.value - 1;
        }
    });
    $( "#messageSlider" ).change(function() {
        slider.slider( "value", this.selectedIndex + 1 );
    });

    splitPost();
});

// single post spliter
splitPost = function(){

    $('.divider').hide();
    var spliterInt = $('.divider').length;
    var containerHeight = 330;
    var maximumHeight = 0;
    var height = [];
    var cutHeight = 0;

    setTimeout(function(){
        for (var i = 0; i < 14; i++) {
            if( imageResize == 1 ){
                height[i] = $('.divider').eq(i).height() - cutHeight + (cutHeight - 10)%13;
                var imageResize = 0;
            } else {
                height[i] = $('.divider').eq(i).height();
            }

            if( maximumHeight + height[i] < containerHeight ){
                maximumHeight = maximumHeight + height[i];
                var box = $('.divider').clone().eq(i).show();
                $('.fillSinglePost').after(box);
                $('.fillSinglePost').next().addClass('fillSinglePost');
                $('.fillSinglePost').eq(0).removeClass('fillSinglePost');


            } else if (height[i] > containerHeight) {

                var times = height[i] / containerHeight;
                times = Math.ceil(times);

                for(var j = 0; j<times; j++){
                    var box = $('.divider').clone().eq(i).css({
                        'height': containerHeight,
                        'overflow' : 'hidden'
                    }).show();
                    if( $('.fillSinglePost').parent().parent().next('.column').length > 0 ){
                        $('.fillSinglePost').parent().parent().next('.column').find('.containerPostings').html(box);
                    } else {
                        $('.fillSinglePost').parent().parent().parent().parent().next().find('.containerPostings').eq(0).html(box);
                    }
                    $('.divider :last').addClass('fillSinglePost');
                    $('.fillSinglePost').eq(0).removeClass('fillSinglePost');
                }
            } else {
                if( containerHeight - maximumHeight > 60 ){
                    cutHeight = containerHeight - maximumHeight;

                    var box = $('.divider').clone().eq(i).css({
                        'height': cutHeight - ((cutHeight - 10)%13),
                        'overflow' : 'hidden'
                    }).show();
                    $('.fillSinglePost').after(box);
                    imageResize = 1;
                } else {
                    imageResize = 0;
                }

                maximumHeight = 0;
                maximumHeight = maximumHeight + height[i];

                var box = $('.divider').clone().eq(i).show();
                if( $('.fillSinglePost').parent().parent().next('.column').length > 0 ){
                    $('.fillSinglePost').parent().parent().next('.column').find('.containerPostings').html(box);
                } else {
                    $('.fillSinglePost').parent().parent().parent().parent().next().find('.containerPostings').eq(0).html(box);
                }

                $('.divider :last').addClass('fillSinglePost');
                $('.fillSinglePost').eq(0).removeClass('fillSinglePost');

                if( imageResize == 1 ){
                    var thisPostHeight = $('.fillSinglePost').height();
                    $('.fillSinglePost').css({
                        'height': thisPostHeight - (cutHeight - ((cutHeight - 10)%13)),
                        'overflow': 'hidden'
                    });
                    $('.fillSinglePost').find('.box').css({
                        'top': - cutHeight + ((cutHeight - 10)%13)
                    });
                }
            }
        }
    },100)
}