var searchHistory = [];
var currentSearchWord = -1;

$(document).ready(function(){

    // back and forward browsing for search/post box

    $('.searchPostBoxInner').blur(function(){
        if($(this).val() != ''){
            searchHistory.push($(this).val());
            currentSearchWord++;
        }
    });

    $('.naviR a').on('click', function(){
        if(typeof searchHistory[currentSearchWord + 1] != 'undefined'){
            currentSearchWord++;
            $('.searchPostBoxInner').val(searchHistory[currentSearchWord]);
        }
    });
    $('.naviL a').on('click', function(){
        if(typeof searchHistory[currentSearchWord - 1] != 'undefined'){
            currentSearchWord--;
            $('.searchPostBoxInner').val(searchHistory[currentSearchWord]);
        }
    });

    // comment box javascript

    $('.komment').on({
        click: function(e){
            e.preventDefault();
            $('.container-right').removeClass('active');
            $('.column').animate({'width': '289px'}, 200);
            $(this).parent().parent().parent().next().find('.container-right').addClass('active');
            $(this).parent().parent().parent().parent().animate({'width': '488px'}, 200);
            $(this).parent().parent().parent().parent().find('.containerPostings').animate({'width': '488px'}, 200);
            $('.menu').hide();
            $('#search-input').focus();

            $('.searchPostBoxInner').on({
                keyup: function(){
                    $('.commentText').text($('.searchPostBoxInner').val());
                }
            });
        }
    });

    // last comment box
    $('.komment-last').on({
        click: function(e){
            e.preventDefault();
            $(this).parent().parent().parent().parent().parent().parent().parent().parent().find('.column').eq(0).animate({'width':'0'}, 200);
        }
    });

    // when clicking on post, hide the comment box and (for now) just alert. Send post to server, then show new post in mediabox

    $('.send-post').on({
        click: function(e){
            e.preventDefault();
            $('.containerPostings').removeClass('active');
            $('.container-right').removeClass('active');
            alert('post sent');
        }
    });

    // Main dropdown menu
    $(document).ready(function () {
        $(".menuArrow").click(function (e) {
            e.preventDefault();
            $('.search').toggleClass('search-active');
            $(".submenuMain").toggle();
        });
    });

    // showing the Intranet menu

    $(document).ready(function()
    {

        $(".intranet").click(function(e)
        {
            e.stopPropagation();
            e.preventDefault();
            var X=$(this).attr('id');
            if(X==1)
            {
                $(".submenu").hide();
                $(this).attr('id', '0');
            }
            else
            {
                $(".submenu").show();
                $(this).attr('id', '1');
            }

        });

        //Mouse click on sub menu
        $(".submenu").mouseup(function()
        {
            return false
        });

        $(".submenu").mouseup(function()
        {
            return false
        });


        //Document Click
        $(document).mouseup(function()
        {
            $(".submenu").hide();
            $(".submenu").attr('id', '');
        });
    });

    // animate indicator for the main menu
    if( $('header nav a.icons-menu-1').length > 0 ){
        var menuItem1 = $('header nav a.icons-menu-1');
        var posLeft = menuItem1.position().left + ( menuItem1.width() / 2 ) - 2;
        $('.menuIndicator').css({
            left: posLeft
        });
    }

        $('header nav a').on({
            click: function(){
                var self = $(this);
                var position = self.position();
                $('.menuIndicator').animate({
                    left: position.left + ( self.width() / 2 ) - 2
                }, 300)

                navigation.changeSearchIcon(self.attr('class'));
            }
        });
    });

    // onclick change class in intranet box
    $('.root a').click(function() {
        if($(this).hasClass('checkLight')){
            $(this).removeClass('checkLight');
            $(this).addClass('checkFull');
        } else if($(this).hasClass('checkFull')){
            $(this).removeClass('checkFull');
            $(this).addClass('checkLight');
            $('.offenCheck a').removeClass('check1').addClass('check2');
            $('.intranetCheck .intranet').removeClass('inactive');
        }
    });

    // public/private posts toggler

    $('.offenCheck a').click(function() {
        if($(this).hasClass('check1')){
            $(this).removeClass('check1');
            $(this).addClass('check2');

        } else if($(this).hasClass('check2')){
            $(this).removeClass('check2');
            $(this).addClass('check1');
            $('.intranetCheck .root li a').removeClass('checkLight').addClass('checkFull');
            $('.intranetCheck .intranet').addClass('inactive');
        }
    });


    // search box popup menu hide/show

    $('body').on("click", function() {
        $('.menuSearch').hide();
        $('.special').hide();
    });

    $('.dropSearch').on({
        click: function(e){
            e.preventDefault();
            e.stopPropagation();
            $('.menuSearch').hide();
            $(this).next().fadeIn('fast');
            if ($(this).parent().nextAll().length < 3){
                $(this).next().css({'top':'-67px'}).addClass('special');
            }
        }
    });

    // date active classes

    $('#mediabox nav a').click(function() {
            $(this).addClass('active');
            $(this).siblings('a').removeClass('active');
    });

    // hide and show the mediabox (open icon still missing)

    $(".media-slider").hide();
    $(".media-slider").show();

    $('.closeTop').click(function(){
        $(".hide-show").stop().slideToggle('fast');
    });

    // magic selectors for mediabox tabs







