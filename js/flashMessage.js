/**
 * Flash messages system for crealiity
 *
 * @author  Florian Rachor <fr@websafari.co>
 */
// Initialize flashMessage object
var flashMessage = {};

flashMessageHandler = function(options) {
    // set up default options
    var defaults = {
        selector:     '.flash-message',
        duration:     500,
        highlightClass:  'message-highlight'
    };

    // Overwrite default options with user provided ones
    var options = $.extend({}, defaults, options);
    // Initialize the element
    var e = $(options.selector);

    flashMessage.show = function(message, highlight) {
        message = message || "This is the default message";
        highlight = highlight || false;

        e.hide();
        if(highlight) e.html(message).addClass(options.highlightClass).fadeIn(options.duration);
        else e.html(message).removeClass(options.highlightClass).fadeIn(options.duration);
    }

    flashMessage.hide = function () {
        e.fadeOut(options.duration);
    }
}

$(document).ready(function(){
    flashMessageHandler();

    // todo: remove for production
    setTimeout("flashMessage.show('Hallo mein Name ist L.i.i.s.a')", 0);
    setTimeout("flashMessage.show('Ich bin die künstliche Intelligenz von Crealiity')", 2000);
    setTimeout("flashMessage.show('Über dieses Feld kann ich mit dir sprechen')", 4000);
    setTimeout("flashMessage.show('Wichtige Nachrichten hebe ich für Dich hervor', true)", 6000);
    setTimeout("flashMessage.show('Viel Spaß beim Verwenden der Seite')", 8000);
    setTimeout("flashMessage.hide()", 10000);
});